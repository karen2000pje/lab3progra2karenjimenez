/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class ObjJugadores {

    private Integer cedula_jugador;
    private String nombre_jugador;
    private Integer edad_jugador;
    private boolean estado;
    private int destreza;
    private int nombre_equipo;
    private String perfil;
    private String N_EQUIPO;
    //tabla destrezas//
    private Date fecha_debug;
    private String cedmodi;

    //tabla equipo//


    public static ArrayList listajugadores = new ArrayList<>();

    //tabla jugadores//

    public ObjJugadores(Integer cedula_jugador, String nombre_jugador, Integer edad_jugador, boolean estado, int destreza, int nombre_equipo, Date fecha_debug) {
        this.cedula_jugador = cedula_jugador;
        this.nombre_jugador = nombre_jugador;
        this.edad_jugador = edad_jugador;
        this.estado = estado;
        this.destreza = destreza;
        this.nombre_equipo = nombre_equipo;
        this.fecha_debug = fecha_debug;
    }

    public ObjJugadores(Integer cedula_jugador, String nombre_jugador, String perfil, String N_EQUIPO) {
        this.cedula_jugador = cedula_jugador;
        this.nombre_jugador = nombre_jugador;
        this.perfil = perfil;
        this.N_EQUIPO = N_EQUIPO;
    }

    public ObjJugadores(String nombre_jugador, String N_EQUIPO, Date fecha_debug) {
        this.nombre_jugador = nombre_jugador;
        this.N_EQUIPO = N_EQUIPO;
        this.fecha_debug = fecha_debug;
    }

    public ObjJugadores(Integer cedula_jugador, String nombre_jugador, Integer edad_jugador, Date fecha_debug) {
        this.cedula_jugador = cedula_jugador;
        this.nombre_jugador = nombre_jugador;
        this.edad_jugador = edad_jugador;
        this.fecha_debug = fecha_debug;
    }

    public ObjJugadores(Integer cedula_jugador, String nombre_jugador, Integer edad_jugador, boolean estado, int destreza, String N_EQUIPO, Date fecha_debug, String cedmodi) {
        this.cedula_jugador = cedula_jugador;
        this.nombre_jugador = nombre_jugador;
        this.edad_jugador = edad_jugador;
        this.estado = estado;
        this.destreza = destreza;
        this.N_EQUIPO = N_EQUIPO;
        this.fecha_debug = fecha_debug;
        this.cedmodi = cedmodi;
    }

    public ObjJugadores(String nombre_jugador, Integer edad_jugador, boolean estado, String perfil, String N_EQUIPO, Date fecha_debug, String cedmodi) {
        this.nombre_jugador = nombre_jugador;
        this.edad_jugador = edad_jugador;
        this.estado = estado;
        this.perfil = perfil;
        this.N_EQUIPO = N_EQUIPO;
        this.fecha_debug = fecha_debug;
        this.cedmodi = cedmodi;//fortaleza
    }

    public ObjJugadores(Integer cedula, String nombre, Integer edad, boolean estado, int numquipo, int iddestreza, Date fecha, String modiced) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ObjJugadores(String nombre, Integer edad, boolean estado, int numquipo, int iddestreza, Date fecha, String modiced) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getCedmodi() {
        return cedmodi;
    }

    public void setCedmodi(String cedmodi) {
        this.cedmodi = cedmodi;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getN_EQUIPO() {
        return N_EQUIPO;
    }

    public void setN_EQUIPO(String N_EQUIPO) {
        this.N_EQUIPO = N_EQUIPO;
    }

    public Integer getCedula_jugador() {
        return cedula_jugador;
    }

    public void setCedula_jugador(Integer cedula_jugador) {
        this.cedula_jugador = cedula_jugador;
    }

    public String getNombre_jugador() {
        return nombre_jugador;
    }

    public void setNombre_jugador(String nombre_jugador) {
        this.nombre_jugador = nombre_jugador;
    }

    public Integer getEdad_jugador() {
        return edad_jugador;
    }

    public void setEdad_jugador(Integer edad_jugador) {
        this.edad_jugador = edad_jugador;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getDestreza() {
        return destreza;
    }

    public void setDestreza(int destreza) {
        this.destreza = destreza;
    }

    public int getNombre_equipo() {
        return nombre_equipo;
    }

    public void setNombre_equipo(int nombre_equipo) {
        this.nombre_equipo = nombre_equipo;
    }

    public Date getFecha_debug() {
        return fecha_debug;
    }

    public void setFecha_debug(Date fecha_debug) {
        this.fecha_debug = fecha_debug;
    }

    public static ArrayList getListajugadores() {
        return listajugadores;
    }

    public static void setListajugadores(ArrayList listajugadores) {
        ObjJugadores.listajugadores = listajugadores;
    }







}
