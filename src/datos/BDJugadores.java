/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

//import datos.BDCconexion.BDConexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import objetos.ObjJugadores;

/**
 *
 * @author Usuario
 */
public class BDJugadores {

    private ResultSet rs = null;
    private Statement s = null;
    BDCconexion conexion = new BDCconexion();
    private Connection connection = null;

    public void insertarJugadores(ObjJugadores jugador) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO jugadores (cedula,nombre,edad,estado,equipo,destreza,fecha_debut) VALUES (" + jugador.getCedula_jugador() + ",'" + jugador.getNombre_jugador() + "'," + jugador.getEdad_jugador() + ",'" + jugador.isEstado() + "'," + jugador.getNombre_equipo() + "," + jugador.getDestreza() + ",'" + jugador.getFecha_debug() + "')");
            if ((z == 1)) {
                JOptionPane.showMessageDialog(null, "Se agregó el registro de manera exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al insertar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public ArrayList cargarnombreEquipos() {
        ArrayList listEquipos = new ArrayList();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select  e.nombre_equipo nome from equipo e");
            while (rs.next()) {
                listEquipos.add(rs.getString("nome"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return listEquipos;
    }

    public ArrayList cargarcedulasjug() {
        ArrayList listEquipos = new ArrayList();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select  j.cedula ced from jugadores j");
            while (rs.next()) {
                listEquipos.add(rs.getString("ced"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return listEquipos;
    }

    public ArrayList<ObjJugadores> cargartablatodosJug() {
        ArrayList<ObjJugadores> listtabla = new ArrayList();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.cedula ced, j.nombre nom, j.edad ed, j.estado est, j.equipo equip, j.destreza dest, j.fecha_debut fechad\n"
                    + "from jugadores j");
            while (rs.next()) {
                listtabla.add(new ObjJugadores(rs.getInt("ced"), rs.getString("nom"), rs.getInt("ed"), rs.getBoolean("est"), rs.getInt("equip"), rs.getInt("dest"), rs.getDate("fechad")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return listtabla;
    }

    public ArrayList<ObjJugadores> cargar1repo() {
        ArrayList<ObjJugadores> listrepo1 = new ArrayList();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.cedula ced, j.nombre nom, e.nombre_equipo nom_equi, d.perfil perf\n"
                    + "from jugadores j\n"
                    + "inner join destrezas d\n"
                    + "on j.destreza= d.id_destreza\n"
                    + "inner join equipo e\n"
                    + "on j.equipo= e.id_equipo\n"
                    + "where j.estado= 'true' and d.fortaleza= 'velocidad' and j.edad >20");
            while (rs.next()) {
                listrepo1.add(new ObjJugadores(rs.getInt("ced"), rs.getString("nom"), rs.getString("perf"), rs.getString("nom_equi")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return listrepo1;
    }

    public ArrayList<ObjJugadores> cargar2repo(Date desde, Date hasta) {
        ArrayList<ObjJugadores> listrepo2 = new ArrayList();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select j.nombre nomj,e.nombre_equipo nomeq,j.fecha_debut fechad\n"
                    + "from jugadores j\n"
                    + "inner join equipo e\n"
                    + "on j.equipo= e.id_equipo\n"
                    + "where j.fecha_debut BETWEEN '" + desde + "' AND '" + hasta + "'");
            while (rs.next()) {
                listrepo2.add(new ObjJugadores(rs.getString("nomj"), rs.getString("nomeq"), rs.getDate("fechad")));
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return listrepo2;
    }

    public ArrayList<ObjJugadores> cargar3repo(String desde, String hasta, String equipo) {
        ArrayList<ObjJugadores> listrepo3 = new ArrayList();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.cedula ced, j.nombre nom, j.edad ed, j.fecha_debut fechad\n"
                    + "from jugadores j\n"
                    + "inner join destrezas d\n"
                    + "on j.destreza= d.id_destreza\n"
                    + "inner join equipo e\n"
                    + "on j.equipo= e.id_equipo\n"
                    + "where j.edad BETWEEN " + desde + " AND " + hasta + " and j.estado= 'true' and e.nombre_equipo ='" + equipo + "' and  d.fortaleza= 'cabeceo' and e.estado= 'true' and j.fecha_debut > '1997-01-01' "
                    + "ORDER BY j.nombre ASC");
            while (rs.next()) {
                listrepo3.add(new ObjJugadores(rs.getInt("ced"), rs.getString("nom"), rs.getInt("ed"), rs.getDate("fechad")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return listrepo3;
    }

    public void modijugadores(ObjJugadores obj) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("Update jugadores j set nombre= '" + obj.getNombre_jugador() + "', edad= " + obj.getEdad_jugador() + ", estado= '" + obj.isEstado() + "', equipo= " + obj.getNombre_equipo() + ",destreza= " + obj.getDestreza() + ", fecha_debut='" + obj.getFecha_debug() + "'\n"
                    + "where j.cedula= " + obj.getCedula_jugador() + ";");

            if ((z == 1)) {
                JOptionPane.showMessageDialog(null, "Se modificó el registro de manera exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al insertar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void eliminarJug(Integer cedula) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int z = s.executeUpdate("DELETE FROM jugadores j\n"
                    + "WHERE j.cedula ='" + cedula + "'");
            if ((z == 1)) {
                JOptionPane.showMessageDialog(null, "Se eliminó el registro de manera exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al eliminar el registro", "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public ObjJugadores infojugadores(String ced) {
        ObjJugadores lista = null;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select j.nombre nom,j.edad ed, j.estado est, e.nombre_equipo nomequi, d.perfil perf, d.fortaleza fort, j.fecha_debut fechad\n"
                    + "from jugadores j\n"
                    + "inner join destrezas d\n"
                    + "on j.destreza= d.id_destreza\n"
                    + "inner join equipo e\n"
                    + "on j.equipo= e.id_equipo\n"
                    + "where j.cedula= " + ced);
            while (rs.next()) {
                lista = (new ObjJugadores(rs.getString("nom"), rs.getInt("ed"), rs.getBoolean("est"), rs.getString("perf"), rs.getString("nomequi"), rs.getDate("fechad"), rs.getString("fort")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return lista;
    }
}
