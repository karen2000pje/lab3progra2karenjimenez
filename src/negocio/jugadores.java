/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.BDJugadores;
import java.util.ArrayList;
import java.util.Date;
import objetos.ObjJugadores;

/**
 *
 * @author Usuario
 */
public class jugadores {

    BDJugadores bdjugador = new BDJugadores();

    public void InsertarJugador(ObjJugadores jugador) {
        bdjugador.insertarJugadores(jugador);
    }

    public ArrayList cargarJugadores() {
        ArrayList listaEquipos = bdjugador.cargarnombreEquipos();
        return listaEquipos;
    }

    public ArrayList cargarcedulas() {
        ArrayList listaEquipos = bdjugador.cargarcedulasjug();
        return listaEquipos;
    }

    public ArrayList<ObjJugadores> cargarrepo1neg() {
        ArrayList<ObjJugadores> listaEquipos = bdjugador.cargar1repo();
        return listaEquipos;
    }

    public ArrayList<ObjJugadores> cargarrepo2neg(Date desde, Date hasta) {
        ArrayList<ObjJugadores> listaEquipos = bdjugador.cargar2repo(desde, hasta);
        return listaEquipos;
    }

    public ArrayList<ObjJugadores> cargarrepo3neg(String desde, String hasta, String equipo) {
        ArrayList<ObjJugadores> listaEquipos = bdjugador.cargar3repo(desde, hasta, equipo);
        return listaEquipos;
    }

    public ArrayList<ObjJugadores> cargatablajug() {
        ArrayList<ObjJugadores> listaEquipos = bdjugador.cargartablatodosJug();
        return listaEquipos;
    }

    public ObjJugadores infojug(String ced) {
        ObjJugadores listaEquipos = bdjugador.infojugadores(ced);
        return listaEquipos;
    }

    public void cargamodi(ObjJugadores listaEquipos) {
        bdjugador.modijugadores(listaEquipos);
    }

    public void eliminafila(Integer ced) {
        bdjugador.eliminarJug(ced);
    }
}
